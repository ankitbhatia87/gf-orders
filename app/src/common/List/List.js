import React from 'react';

import {FlatList, View, Text} from 'react-native';

/* Components */
import ItemOrder from './Item/ItemOrder';
import Constant from '../../Constant/Constant';
import ItemProduct from './Item/ItemProduct';

export default class List extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      listType: props.listType,
    };
  }

  /* private methods start */

  _renderFlatList() {
    

    switch (this.state.listType) {
      case Constant.constant.ORDER_LIST:
        //console.log("data = ", this.props.source);
        return (
          <FlatList
            data={this.props.source}
            renderItem={data => (
              <ItemOrder
                keyExtractor={data.item.index}
                id={data.item.id}
                createdOn={data.item.createdOn}
                updatedOn={data.item.updatedOn}
                deliveryStatus={data.item.deliveryStatus}
                order={data.item.productsList}
                navigate={this.props.navigate}
              />
            )}
          />
        );
      case Constant.constant.PRODUCTS_LIST:
        return (
          <FlatList
            data={this.props.source}
            renderItem={data => (
              <ItemProduct
                keyExtractor={data.index}
                productName={data.item.productName}
                productCategory={data.item.productCategory}
                productCost={data.item.productCost}
                shippingCost={data.item.shippingCost}
                quantity={data.item.quantity}
                salePricePerItem={data.item.salePricePerItem}
              />
            )}
          />
        );
    }
  }
  /* private methods end */
  render() {
    this._flatList = this._renderFlatList();

    return (
      <View>
        {this._flatList}
      </View>
    );
  }
}
