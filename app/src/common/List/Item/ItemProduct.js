import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

export default class ItemProduct extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      id: props.id,
      productName: props.productName,
      productCategory: props.productCategory,
      productCost: props.productCost,
      shippingCost: props.shippingCost,
      quantity: props.quantity,
      salePricePerItem: props.salePricePerItem,
    };
  }
  render() {
    return (
      <View style={styles.item_container} id={this.state.id}>
        <Text>Product List</Text>
        <Text style={styles.heading}>{this.state.productName}</Text>
        <View style={styles.item_details}>
          <View class={styles.customer_info}>
            {/* <Text>Name: {this.state.productName}</Text> */}
            <Text>Quantity: {this.state.quantity}</Text>
            <Text>SP: {this.state.salePricePerItem}</Text>
            <Text>Category: {this.state.productCategory}</Text>
            <Text>MRP: {this.state.productCost}</Text>
            <Text>Shipping COst: {this.state.shippingCost}</Text>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  item_container: {
    flex: 1,
  },
  heading: {
    fontSize: 30,
  },
  item_details: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  customer_info: {},
});
