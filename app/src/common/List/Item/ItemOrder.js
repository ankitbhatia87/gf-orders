import React from 'react';
import {connect} from 'react-redux';
import {View, Text, StyleSheet, TouchableOpacity, Animated} from 'react-native';
import Swipeable from 'react-native-gesture-handler/Swipeable';
import { deleteOrderUserClick, editOrderUserClick } from '../../../actions/orderAction';

class ItemOrder extends React.Component {
  LeftActions = (progress, dragX) => {
    const scale = dragX.interpolate({
      inputRange: [0, 100],
      outputRange: [0, 1],
      extrapolate: 'clamp',
    });
    return (
      <TouchableOpacity onPress={() => console.log('Edit button pressed')}>
        <View style={{backgroundColor: 'green', justifyContent: 'center'}}>
          <Animated.Text
            style={{
              color: 'white',
              paddingHorizontal: 10,
              fontWeight: '600',
              transform: [{scale}],
            }}>
            Edit
          </Animated.Text>
        </View>
      </TouchableOpacity>
    );
  };

  RightActions = (progress, dragX) => {
    const scale = dragX.interpolate({
      inputRange: [-100, 0],
      outputRange: [0.7, 0],
    });
    return (
      <TouchableOpacity onPress={() => console.log('Delete button pressed')}>
        <View style={{backgroundColor: 'red', justifyContent: 'center'}}>
          <Animated.Text
            style={{
              color: 'white',
              paddingHorizontal: 10,
              fontWeight: '600',
              transform: [{scale}],
            }}>
            Delete
          </Animated.Text>
        </View>
      </TouchableOpacity>
    );
  };

  _openProductDetail(navigate, order) {
    navigate.navigate('ShowProduct', {productList: order});
  }

  bgColorCode = deliveryStatus => {
    switch (deliveryStatus) {
      case 0:
        return {
          flex: 1,
          backgroundColor: 'orange',
        };
      case 1:
        return {
          flex: 1,
          backgroundColor: 'yellow',
        };
      case 2:
        return {
          flex: 1,
          backgroundColor: 'green',
        };
      case 3:
        return {
          flex: 1,
          backgroundColor: 'red',
        };
      case 4:
        return {
          flex: 1,
          backgroundColor: 'grey',
        };
      default:
        return {
          flex: 1,
          backgroundColor: '',
        };
    }
  };

  render() {
    const {navigate, order, id} = this.props;

    //console.log('order = ', order);
    let totalSale = 0;

    for (let index = 0; index < order.length; index++) {
      const element = order[index];
      totalSale = +totalSale + +element.salePricePerItem;
    }

    return (
      <Swipeable
        renderLeftActions={this.LeftActions}
        renderRightActions={this.RightActions}>
        <View style={this.bgColorCode(this.props.deliveryStatus)} id={id}>
          <Text
            onPress={this._openProductDetail.bind(this, navigate, order)}
            style={styles.heading}>
            Order Name - TBD
          </Text>
          <Text>TotalSale: {totalSale}</Text>
        </View>
      </Swipeable>
    );
  }
}

const styles = StyleSheet.create({
  item_container: {
    flex: 1,
  },
  heading: {
    fontSize: 20,
    textAlign: 'center',
  },
  item_details: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  customer_info: {},
});

export default connect(
  '',
  {editOrderUserClick},
)(ItemOrder);
