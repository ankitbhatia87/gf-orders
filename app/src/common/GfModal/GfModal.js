import React, {Component} from 'react';
import {View, Text, Modal, TouchableHighlight} from 'react-native';
import Constant from '../../Constant/Constant';
import AddProduct from '../../components/AddOrder/AddProduct/AddProduct';
import AddCustomer from '../../components/AddCustomer/AddCustomer';

export default class GfModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showHideModal: props.showHideModalProp,
      view: props.view,
    };
  }

  /* private methods start */
  _getView(view) {
    switch (view) {
      case Constant.constant.ADD_CUSTOMER:
        return (
          <AddCustomer
            showHideModalProp={this.state.showHideModal.bind(this)}
          />
        );
      case Constant.constant.ADD_PRODUCT:
        return (
          <AddProduct
            showHideModalProp={this.state.showHideModal.bind(this)}
          />
        );
    }
  }
  /* private methods end */

  render() {
    const viewType = this._getView(this.state.view);
    return (
      <View>
        <Modal
          animationType="slide"
          transparent={false}
          visible={this.props.visibility}>
          {viewType}
          <TouchableHighlight
            onPress={() => {
              this.state.showHideModal(false);
            }}>
            <Text>Hide Modal</Text>
          </TouchableHighlight>
        </Modal>
      </View>
    );
  }
}
