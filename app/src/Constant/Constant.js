export default class Constant {
  static constant = {
    ORDER_LIST: 'ORDER_LIST',
    PRODUCTS_LIST: 'PRODUCT_LIST',
    CUSTOMERS: 'customers',
    ADD_CUSTOMER: 'add:customer',
    ADD_PRODUCT: 'add:product',
    ADD_ORDER: 'add:order',
    ORDERS: 'orders',
    CATEGORIES: 'categories',
    PRODUCTS: 'products',
    CUSTOMER_ID: 'customerId'
  };
}
