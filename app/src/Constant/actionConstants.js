const GET_CUSTOMER_LIST = "get_customer_list"
const GET_ORDER_LIST = "get_order_list"
const ADD_PRODUCT = 'add_product'
const GET_CATEGORIES = "get_categories"
const RESET_ORDER_LIST = "reset_order_list"

export {
    GET_CUSTOMER_LIST,
    GET_ORDER_LIST,
    ADD_PRODUCT,
    GET_CATEGORIES,
    RESET_ORDER_LIST
}