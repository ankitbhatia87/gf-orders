import * as actionConstants from '../Constant/actionConstants';
import Constant from '../Constant/Constant';
import Core from '../../../server/core';

export const addProduct = data => (dispatch, getState) => {

  // console.log('add product data in action = ', data);

var productListArray = [...getState().productList.productList]

// console.log("before Push productListArray = ", productListArray);

productListArray.push(data);

// console.log("after Push productListArray = ", productListArray);


  dispatch({
    type: actionConstants.ADD_PRODUCT,
    payload: productListArray,
  });
};


export const loadCategories = () => (dispatch, getState) => {
  Core.load({
    entity: Constant.constant.CATEGORIES,
  }).then(categories => {
    dispatch({
      type: actionConstants.GET_CATEGORIES,
      payload: categories
    })
  });
}
