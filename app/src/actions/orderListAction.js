import * as actionConstants from '../Constant/actionConstants';
import Constant from '../Constant/Constant';
import Core from '../../../server/core';

export const loadCustomersList = () => (dispatch, getState) => {
  Core.load({
    entity: Constant.constant.CUSTOMERS,
  })
    .then(customers => {
      console.log('customers in action = ', customers);

      dispatch({
        type: actionConstants.GET_CUSTOMER_LIST,
        payload: customers,
      });
    })
    .catch(err => {
      console.log(err);
    });
};

export const loadOrderList = (selectedCustomerId) => (dispatch, getState) => {
  Core.loadById({
    entity: Constant.constant.ORDERS,
    queryKey: Constant.constant.CUSTOMER_ID,
    id: selectedCustomerId,
  })
    .then(ordersList => {
      // console.log('orders in action = ', ordersList);

      dispatch({
        type: actionConstants.GET_ORDER_LIST,
        payload: ordersList,
      });
    })
    .catch(err => {
      console.log(err);
    });
};

export const resetOrderList = () => (dispatch, getState) => {
  

      dispatch({
        type: actionConstants.RESET_ORDER_LIST,
      });
    
};
