import * as actionConstants from '../Constant/actionConstants';
import Constant from '../Constant/Constant';
import Core from '../../../server/core';

export const addOrderUserClick = (data) => (dispatch, getState) => {
    console.log("data in action = ", data);
    
    Core.add({
        entity: Constant.constant.ORDERS,
        payload: data,
      })
        .then(result => {
            console.log(`Order Successfully Saved! ${result}`);
            // dispatch({
            //     type: actionConstants.ADD_ORDER,
            //     payload: result,
            //   });
        })
        .catch(err => console.error(err));
};


export const editOrderUserClick = (data) => (dispatch, getState) => {
  console.log("data in action = ", data);
  
  Core.add({
      entity: Constant.constant.ORDERS,
      payload: data,
    })
      .then(result => {
          console.log(`Order Successfully Saved! ${result}`);
          // dispatch({
          //     type: actionConstants.ADD_ORDER,
          //     payload: result,
          //   });
      })
      .catch(err => console.error(err));
};


export const deleteOrderUserClick = (data) => (dispatch, getState) => {
  console.log("data in action = ", data);
  
  Core.add({
      entity: Constant.constant.ORDERS,
      payload: data,
    })
      .then(result => {
          console.log(`Order Successfully Saved! ${result}`);
          // dispatch({
          //     type: actionConstants.ADD_ORDER,
          //     payload: result,
          //   });
      })
      .catch(err => console.error(err));
};