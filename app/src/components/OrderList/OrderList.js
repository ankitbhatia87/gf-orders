import React from 'react';
import {connect} from 'react-redux';
import {
  Button,
  View,
  StyleSheet,
  SafeAreaView,
  Picker,
  RefreshControl,
  Text,
} from 'react-native';
import List from '../../common/List/List';
import Constant from '../../Constant/Constant';
import Core from '../../../../server/core';
import GfModal from '../../common/GfModal/GfModal';
import {ScrollView} from 'react-native-gesture-handler';
import {
  loadCustomersList,
  loadOrderList,
  resetOrderList,
} from '../../actions/orderListAction';

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  listContainer: {
    flex: 10,
    paddingLeft: 20,
    paddingRight: 20,
  },
  picker: {
    borderColor: 'green',
    borderWidth: 4,
    flex:1
  }
});

class OrderList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      gfModalVisible: false,
      selectedCustomerId: '',
      isDataUpdated: false,
      refreshing: false,
    };
  }

  /* private methods start */

  _setSelectedCustomer(selectedCustomerParam) {
    const {customers, loadOrderList, ordersList} = this.props;
    console.log('selectedCustomerParam = ', selectedCustomerParam);

    if (selectedCustomerParam === 'addCustomer') {
      this._showHideModal(true);
    } else {
      let customerData = {};

      for (let index = 0; index < customers.length; index++) {
        const element = customers[index];
        if (element.id == selectedCustomerParam) {
          // console.log('if element = ', element);
          customerData = element;
        }
      }

      console.log('customer Data = ', customerData);

      this.setState(
        {
          selectedCustomerId: selectedCustomerParam,
        },
        () => {
          loadOrderList(this.state.selectedCustomerId);
          if (this.state.selectedCustomerId != 'default') {
            this._orderList = (
              <View>
                <Text>Customer Id: {this.state.selectedCustomerId}</Text>
                <Text>Name: {customerData.name}</Text>
                <Text>Address: {customerData.address}</Text>
                <Text>Society: {customerData.society}</Text>
                <Text>Order List</Text>
              </View>
            );
          }
        },
      );
    }
  }

  _showHideModal(visibility) {
    console.log('inside showHideModal visibility = ', visibility);

    this.setState({
      gfModalVisible: visibility,
    });
  }

  _onRefresh() {
    this.setState({
      refreshing: true,
    });
    this.props.loadCustomersList();
    this.props.resetOrderList();
    this.setState({
      refreshing: false,
      selectedCustomerId: 'default',
    });
  }

  _addNewOrder() {
    this.state.selectedCustomerId !== '' &&
      this.props.navigation.navigate('AddOrder', {
        customerId: this.state.selectedCustomerId,
        navigation: this.props.navigation,
      });
  }

  /* private methods end */

  /* Lifecycle Methods start */
  componentDidMount() {
    console.log('inside componentDidMount');

    this.props.loadCustomersList();
    this.props.resetOrderList();
  }

  componentWillUnmount() {
    console.log('inside componentWillUnmount');
  }

  componentDidUpdate(prevProps, prevState) {
    console.log('inside componentDidUpdate');

    if (
      prevState.selectedCustomerId != '' &&
      this.state.selectedCustomerId == ''
    ) {
      console.log('selectedCustomerId = ', prevState.selectedCustomerId);

      this.setState({selectedCustomerId: prevState.selectedCustomerId});
    }
  }

  render() {
    const {customers, ordersList, productsList} = this.props;

    const pickerItems = [];
    customers.map(customer => {
      pickerItems.push(
        <Picker.Item
          label={customer.name}
          value={customer.id}
          key={customer.id}
        />,
      );
    });
    return (
      <SafeAreaView style={styles.container}>
        <ScrollView
          style={{flex: 1}}
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={this._onRefresh.bind(this)}
            />
          }>
          <View style={{flex: 1, flexDirection: 'row'}}>
            <Picker
              style={{flex: 1}}
              selectedValue={this.state.selectedCustomerId}
              onValueChange={this._setSelectedCustomer.bind(this)}>
              <Picker.Item label="Select a Customer" value="default" style={{flex: 1}} />
              {pickerItems}
              <Picker.Item
                label="Add a Customer"
                value="addCustomer"
                // onPress={this._showHideModal.bind(this, true)}
              />
            </Picker>
            {/* <Button
              value="+"
              title="+"
              styles={{flex: 1, height: 10}}
              onPress={this._showHideModal.bind(this, true)}
            /> */}
            <GfModal
              visibility={this.state.gfModalVisible}
              view={Constant.constant.ADD_CUSTOMER}
              showHideModalProp={this._showHideModal.bind(this)}
            />
          </View>
          <View style={styles.listContainer}>
            {this._orderList}
            <List
              navigate={this.props.navigation}
              source={ordersList}
              listType={Constant.constant.ORDER_LIST}
            />
          </View>
        </ScrollView>
        <Button title="Add New Order" onPress={this._addNewOrder.bind(this)} />
      </SafeAreaView>
    );
  }

  /* Lifecycle Methods ends */
}

const actionCreators = {
  loadCustomersList,
  loadOrderList,
  resetOrderList,
};

const mapStateToProps = state => ({
  customers: state.orderList.customers,
  ordersList: state.orderList.orders,
  productsList: state.productList.productList,
});

export default connect(
  mapStateToProps,
  actionCreators,
)(OrderList);
