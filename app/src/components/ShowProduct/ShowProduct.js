import React from 'react';
import {ScrollView, Button, View, SafeAreaView, Text} from 'react-native';

/* External libraries */
import DatePicker from 'react-native-datepicker';
import moment from 'moment';

/* Components */
import List from '../../common/List/List';
import Constant from '../../Constant/Constant';
import ItemProduct from '../../common/List/Item/ItemProduct';


class ShowProduct extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      productList: props.navigation.state.params.productList,
    };    
  }

  render() {
    return (
      <SafeAreaView style={{flex: 1}}>
        <ScrollView style={{flex: 1}}>
          <View style={{flex: 1, backgroundColor: '#ccc'}}>
            <List
              source={this.state.productList}
              listType={Constant.constant.PRODUCTS_LIST}
            />
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

export default ShowProduct;
