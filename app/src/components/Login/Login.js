import React from 'react';
import Core from '../../../../server/core';
import {View, TextInput, StyleSheet, Button} from 'react-native';

class Login extends React.Component {
  state = {
    email: '',
    password: '',
  };

  onLoginBtnPress() {
    const {navigate} = this.props.navigation;
    let email = this.state.email;
    let password = this.state.password;
    Core.authenticate(email, password)
      .then(result => {
        navigate('OrderList');
      })
      .catch(err => {
        console.log(err);
      });
  }

  _getEmail(email) {
    this.setState({
      email: email,
    });
  }

  _getPassword(password) {
    this.setState({
      password: password,
    });
  }

  render() {
    return (
      <View style={styles.container}>
        <TextInput
          style={styles.input}
          placeholder="Email"
          onChangeText={this._getEmail.bind(this)}
        />
        <TextInput
          style={styles.input}
          secureTextEntry={true}
          placeholder="Password"
          onChangeText={this._getPassword.bind(this)}
        />
        <Button title="Login" onPress={this.onLoginBtnPress.bind(this)} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  input: {
    borderColor: '#000',
    borderWidth: 1,
    borderRadius: 30,
    width: 250,
    paddingLeft: 30,
    paddingRight: 30,
    marginBottom: 20,
  },
});

export default Login;
