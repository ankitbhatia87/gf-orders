import React, {Component} from 'react';
import {connect} from 'react-redux';
import {
  SafeAreaView,
  Button,
  StyleSheet,
  TextInput,
  Picker,
  Text,
} from 'react-native';

import {addProduct, loadCategories} from '../../../actions/productListAction';

class AddProduct extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      selectedCategory: 'uncategorized',
      showHideModal: props.showHideModalProp,
      // addProductFn: props.options.addProductFnProp,
    };
  }

  /* Private methods start */
  _onAddProduct() {
    const productInfo = {
      productName: this._productName._lastNativeText,
      productCategory: this._picker.props.selectedValue,
      productCost: this._productCost._lastNativeText,
      shippingCost: this._shippingCost._lastNativeText,
      quantity: this._quantity._lastNativeText,
      salePricePerItem: this._salePricePerItem._lastNativeText,
    };

    this.props.addProduct(productInfo);

    // this.state.addProductFn(productInfo);
    this.state.showHideModal(false);
  }

  _setSelectedCategory(selectedVal) {
    this.setState({
      selectedCategory: selectedVal,
    });
  }

  /* Private methods end */

  /* Lifecycle methods start */

  componentDidMount() {
    this.props.loadCategories();
  }

  render() {
    const pickerItems = [];
    this.props.categories.map(category => {
      pickerItems.push(
        <Picker.Item
          label={category.name}
          value={category.value}
          key={category.id}
        />,
      );
    });
    return (
      <SafeAreaView style={{flex: 1}}>
        <TextInput
          style={styles.input}
          ref={val => (this._productName = val)}
          placeholder="Product Name"
        />
        <Picker
          ref={picker => (this._picker = picker)}
          selectedValue={this.state.selectedCategory}
          onValueChange={this._setSelectedCategory.bind(this)}>
          <Picker.Item label="Select a Category" value="uncategorized" />
          {pickerItems}
        </Picker>
        <TextInput
          style={styles.input}
          ref={val => (this._productCost = val)}
          placeholder="Product Cost"
        />
        <TextInput
          style={styles.input}
          ref={val => (this._shippingCost = val)}
          placeholder="Shipping Cost"
        />
        <TextInput
          style={styles.input}
          ref={val => (this._quantity = val)}
          placeholder="Quantity"
        />
        <TextInput
          style={styles.input}
          ref={val => (this._salePricePerItem = val)}
          placeholder="Sale Price per Item"
        />
        <Button title="Add Product" onPress={this._onAddProduct.bind(this)} />
        <Button title="Cancel" />
      </SafeAreaView>
    );
  }
  /* Lifecycle methods end */
}

const styles = StyleSheet.create({
  input: {
    borderColor: '#000',
    borderWidth: 1,
    borderRadius: 30,
    width: 250,
    paddingLeft: 30,
    paddingRight: 30,
    marginBottom: 20,
  },
});

const actionCreators = {
  addProduct,
  loadCategories,
};

const mapStateToProps = state => ({
  categories: state.productList.categories,
});

export default connect(
  mapStateToProps,
  actionCreators,
)(AddProduct);
