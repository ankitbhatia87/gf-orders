import React from 'react';
import {connect} from 'react-redux';
import {
  ScrollView,
  Button,
  View,
  SafeAreaView,
  Text,
  Picker,
  BackHandler,
} from 'react-native';

/* External libraries */
import DatePicker from 'react-native-datepicker';
import moment from 'moment';

/* Components */
import GfModal from '../../common/GfModal/GfModal';
import Constant from '../../Constant/Constant';
import List from '../../common/List/List';
import Core from '../../../../server/core';
import {addOrderUserClick} from '../../actions/orderAction';

const deliveryStatusItems = [
  {
    label: 'accepted',
    value: 0,
  },
  {
    label: 'in process',
    value: 1,
  },
  {
    label: 'delivered',
    value: 2,
  },
  {
    label: 'payment pending',
    value: 3,
  },
  {
    label: 'cancelled',
    value: 4,
  }
];

class AddOrder extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      productsList: [],
      productsLength: 0,
      modalVisible: false,
      createdOn: moment(new Date()).format('YYYY-MM-DD'),
      customerId: props.navigation.state.params.customerId,
      navigation: props.navigation.state.params.navigation,
      orderId: null,
      gfModalVisible: false,
      deliveryStatus: 0,
    };

    // this._onSaveOrder.bind(this);
  }

  /* lifecycle methods start */

  // componentWillMount() {
  //   BackHandler.addEventListener(
  //     'hardwareBackPress',
  //     this._onSaveOrder,
  //   );
  // }

  // componentWillUnmount() {
  //   BackHandler.removeEventListener(
  //     'hardwareBackPress',
  //     this._onSaveOrder,
  //   );
  // }

  /* lifecycle methods end */

  /* Private Methods Start */
  _setDeliveryStatus(value) {
    this.setState({deliveryStatus: value});
  }

  _getProductName(value) {
    this.setState({
      productName: value,
    });
  }

  _showHideModal(visibility) {
    this.setState({
      gfModalVisible: visibility,
    });
  }
  /* Private Methods End */

  /* Handler Methods Start */
  _onAddProductUserClick() {
    this.setState({
      gfModalVisible: true,
    });
  }

  _onAddProduct(productData) {
    let _productsList = this.state.productsList.slice();
    _productsList.push(productData);
    this.setState({
      productsList: _productsList,
    });
    this._productsListView = (
      <List source={_productsList} listType={Constant.constant.PRODUCTS_LIST} />
    );
  }

  _onSaveOrder() {
    console.log("productlist = ", this.state.productsList);
    this._finalOrder = {
      id: this.state.orderId,
      customerId: this.state.customerId,
      productsList: this.state.productsList,
      createdOn: this.state.createdOn,
      deliveryStatus: this.state.deliveryStatus,
    };

    console.log('this._finalOrder = ', this._finalOrder);

    this.props.addOrderUserClick(this._finalOrder);

    // this.state.navigation.goBack(null);
  }
  /* Handler Methods End */
  render() {
    const pickerItems = [];
    deliveryStatusItems.map(statusCode => {
      pickerItems.push(
        <Picker.Item
          label={statusCode.label}
          value={statusCode.value}
          key={statusCode.value}
        />,
      );
    });
    return (
      <SafeAreaView style={{flex: 1}}>
        <ScrollView style={{flex: 1}}>
          <DatePicker
            style={{width: 180, marginBottom: 10, marginTop: 10}}
            date={this.state.date}
            mode="date"
            placeholder="select date"
            format="YYYY-MM-DD"
            confirmBtnText="Confirm"
            cancelBtnText="Cancel"
            customStyles={{
              dateIcon: {
                position: 'absolute',
                right: 0,
                top: 4,
                marginRight: 0,
              },
              dateInput: {
                marginLeft: 36,
                paddingRight: 30,
                paddingLeft: 0,
              },
            }}
            onDateChange={date => {
              this.setState({date: date});
            }}
          />
          <Button
            title="Add Product"
            onPress={this._onAddProductUserClick.bind(this)}
          />

          <GfModal
            visibility={this.state.gfModalVisible}
            view={Constant.constant.ADD_PRODUCT}
            options={{
              addProductFnProp: this._onAddProduct.bind(this),
            }}
            showHideModalProp={this._showHideModal.bind(this)}
          />
          <View style={{flex: 1, backgroundColor: '#ccc'}}>
            {this._productsListView}
          </View>
        </ScrollView>
        <Picker
          selectedValue={this.state.deliveryStatus}
          onValueChange={this._setDeliveryStatus.bind(this)}>
          <Picker.Item label="Select a Delivery Status" value="uncategorized" />
          {pickerItems}
        </Picker>
        <Button title="Save Order" onPress={this._onSaveOrder.bind(this)} />
      </SafeAreaView>
    );
  }
}

export default connect(
  '',
  {addOrderUserClick},
)(AddOrder);
