import React, {Component} from 'react';
import {View, StyleSheet, TextInput, Button} from 'react-native';
import Core from '../../../../server/core';
import Constant from '../../Constant/Constant';

export default class AddCustomer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showHideModal: props.showHideModalProp,
      // callback: props.callback
    };
  }

  /* private methods start */
  _onCreate() {
    const customerInfo = {
      name: this._customerName._lastNativeText,
      address: this._customerAddress._lastNativeText,
      society: this._societyName._lastNativeText,
    };
    Core.add({
      entity: Constant.constant.CUSTOMERS,
      payload: customerInfo,
    })
      .then(result => {
        // Alert.alert('', 'Data Successfully Saved', {cancelable: true});
        console.log(result);
        this.state.showHideModal(false);
        // this.state.callback();
      })

      .catch(err => {
        console.log(err);
        // Alert.alert('', err, {cancelable: true});
      });
  }

  /* private methods end */ 

  render() {
    return (
      <View>
        <TextInput
          style={styles.input}
          placeholder="Customer Name"
          ref={val => (this._customerName = val)}
        />
        <TextInput
          style={styles.input}
          ref={val => (this._customerAddress = val)}
          placeholder="Customer Address"
        />
        <TextInput
          style={styles.input}
          ref={val => (this._societyName = val)}
          placeholder="Society Name"
        />
        <Button
          label="Create"
          title="Create"
          onPress={this._onCreate.bind(this)}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  input: {
    borderColor: '#000',
    borderWidth: 1,
    borderRadius: 30,
    width: 250,
    paddingLeft: 30,
    paddingRight: 30,
    marginBottom: 20,
  },
});
