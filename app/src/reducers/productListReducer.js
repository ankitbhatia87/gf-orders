import * as actionConstants from '../Constant/actionConstants';

const INITIAL_STATE = {
  productList: [],
  categories: []
};

//default home reducer to setup redux
export const ProductListReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case actionConstants.ADD_PRODUCT:
      // console.log('add product reducer = ', action.payload);

      return {...state, productList: action.payload};
      case actionConstants.GET_CATEGORIES:
      // console.log('add product reducer = ', action.payload);

      return {...state, categories: action.payload};
    default:
      return state;
  }
};
