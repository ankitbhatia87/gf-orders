import * as actionConstants from '../Constant/actionConstants';

const INITIAL_STATE = {
  customers: [],
  orders: [],
};

//default home reducer to setup redux
export const OrderListReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case actionConstants.GET_CUSTOMER_LIST:
      // console.log('customer reducer = ', action.payload);

      return {...state, customers: action.payload};
    case actionConstants.GET_ORDER_LIST:
      // console.log('order reducer = ', action.payload);

      return {...state, orders: action.payload};
    default:
      return state;
  }
};
