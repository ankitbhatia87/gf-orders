import { combineReducers } from "redux";
import { OrderListReducer as orderList } from "./orderListReducer"
import { ProductListReducer as productList } from "./productListReducer"
import * as actionConstants from "../Constant/actionConstants"

const appReducers = combineReducers({
  orderList,
  productList,
});

const rootReducer = (state, action) => {

 // when a logout action is dispatched it will reset redux state
 if (action.type === actionConstants.RESET_ORDER_LIST) {
  state.orderList = undefined;
}

  return appReducers(state, action);
};

export default rootReducer;
