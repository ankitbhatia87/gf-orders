/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import OrderList from './app/src/components/OrderList/OrderList';
import AddOrder from './app/src/components/AddOrder/AddOrder';
import ShowProduct from './app/src/components/ShowProduct/ShowProduct';
import Login from './app/src/components/Login/Login';

const MainNavigator = createStackNavigator(
  {
    Login: {screen: Login},
    OrderList: {screen: OrderList},
    AddOrder: {screen: AddOrder},
    ShowProduct: {screen: ShowProduct},
  },
  {
    initialRouteName: 'OrderList',
  },
);

const App = createAppContainer(MainNavigator);

export default App;
