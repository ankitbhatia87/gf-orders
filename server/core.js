import firebase from './Firebase';
require('firebase/firestore');

const db = firebase.firestore();

class Core {
  constructor() {
    this.result = [];
  }
  authenticate(email, password) {
    return firebase.auth().signInWithEmailAndPassword(email, password);
  }

  loadById(options) {
    this.result = [];
    let opts = Object.assign({}, options);
    return db
      .collection(opts.entity)
      .where(opts.queryKey, '==', opts.id)
      .get()
      .then(data => {
        data.forEach(doc => {
          let document = doc.data();
          document.id = doc.id;
          this.result.push(document);
        });
        return this.result;
      })
      .catch(err => {
        Promise.reject(err);
      })
  }

  load(options) {
    this.result = [];
    let opts = Object.assign({}, options);
    return db
      .collection(opts.entity)
      .get()
      .then(data => {
        data.forEach(doc => {
          let document = doc.data();
          document.id = doc.id;
          this.result.push(document);
        });
        return this.result;
      })
      .catch(err => {
        return Promise.reject(error);
      });
  }

  add(options) {
    let opts = Object.assign({}, options);
    return db
      .collection(opts.entity)
      .doc(opts.id || '')
      .set(opts.payload, {merge: true})
      .catch(err => {
        return Promise.reject(err)
      });
  }

  update(options) {
    this.add(options);
  }

  delete(options) {
    let opts = Object.assign({}, options);
    return 
  }
}

export default new Core();
