import firebase from 'firebase';
require('firebase/firestore');

const config = {
  apiKey: 'AIzaSyBGo9iMGeQzLSyITFlp_vtJgES9jMqzlxA',
  authDomain: 'glitzforever-orders.firebaseapp.com',
  databaseURL: 'https://glitzforever-orders.firebaseio.com/',
  projectId: 'glitzforever-orders',
  storageBucket: 'glitzforever-orders.appspot.com',
  messagingSenderId: '736000913065',
  appId: '1:736000913065:web:b827e6a7d15dee214803b8',
  measurementId: 'G-N24F9KGCNJ',
};

firebase.initializeApp(config);

export default firebase;
