/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import React from 'react';
import {name as appName} from './app.json';
import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import rootReducer from './app/src/reducers/index'
import thunk from "redux-thunk";

const store = rootReducer && createStore(rootReducer, applyMiddleware(thunk))

console.ignoredYellowBox = ['Warning:', 'Setting a timer'];
console.disableYellowBox = true;

const Root = () => (
    <Provider store={store}>
      <App />
    </Provider>
  )

AppRegistry.registerComponent(appName, () => Root);
